package org.fasttrackit;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FooterTest {
    Page page = new Page();

    public FooterTest() {
    }

    @Test
    public void user_can_refresh_the_homepage() {
        Footer footer = new Footer();
        footer.clickOnTheRefreshIcon();
        Assert.assertEquals("HomePage", "HomePage", "Expected to be on the HomePage after clicking on the refresh icon");
    }

    @Test
    public void user_can_click_on_the_help_icon() {
        Footer footer = new Footer();
        footer.clickOnTheHelpIcon();
        Assert.assertEquals("User can click on the help button", "User can click on the help button", "Help Icon opens the help window");
    }
}
