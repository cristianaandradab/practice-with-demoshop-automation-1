
package org.fasttrackit;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ProductSortTest {
    Page page = new Page();
    ProductCards productList = new ProductCards();



    @BeforeClass
    public void setup() {
        this.page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickOnTheResetButton();
    }

    @Test
    public void when_sorting_products_a_to_z_products_are_sorted_alphabetically_ASC() {
        Product firstProductBeforeSort = this.productList.getFirstProductInList();
        Product lastProductBeforeSort = this.productList.getLastProductInList();
        this.productList.clickOnTheSortButton();
        this.productList.clickOnTheAZSortButton();
        Product firstProductAfterSort = this.productList.getFirstProductInList();
        Product lastProductAfterSort = this.productList.getLastProductInList();
        Assert.assertEquals(firstProductAfterSort.getTitle(), firstProductBeforeSort.getTitle());
        Assert.assertEquals(lastProductAfterSort.getTitle(), lastProductBeforeSort.getTitle());
    }

    @Test
    public void when_sorting_products_Z_to_A_products_are_sorted_alphabetically_DESC() {
        Product firstProductBeforeSort = this.productList.getFirstProductInList();
        Product lastProductBeforeSort = this.productList.getLastProductInList();
        this.productList.clickOnTheSortButton();
        this.productList.clickOnTheZASortButton();
        Product firstProductAfterSort = this.productList.getFirstProductInList();
        Product lastProductAfterSort = this.productList.getLastProductInList();
        Assert.assertEquals(firstProductAfterSort.getTitle(), lastProductBeforeSort.getTitle());
        Assert.assertEquals(lastProductAfterSort.getTitle(), firstProductBeforeSort.getTitle());
    }

    @Test
    public void when_sorting_products_by_price_low_to_high_products_are_sorted_by_price_ASC() {
        this.productList.clickOnTheSortButton();
        this.productList.clickOnTheSortByPriceLoHi();
        Product firstProductBeforeSort = this.productList.getFirstProductInList();
        Product lastProductBeforeSort = this.productList.getLastProductInList();
        this.productList.clickOnTheSortButton();
        this.productList.clickOnTheSortByPriceLoHi();
        Product firstProductAfterSort = this.productList.getFirstProductInList();
        Product lastProductAfterSort = this.productList.getLastProductInList();
        Assert.assertEquals(firstProductAfterSort.getPrice(), firstProductBeforeSort.getPrice());
        Assert.assertEquals(lastProductAfterSort.getPrice(), lastProductBeforeSort.getPrice());
    }

    @Test
    public void when_sorting_products_by_price_high_to_low_products_are_sorted_by_price_DESC() {
        this.productList.clickOnTheSortButton();
        this.productList.clickOnTheSortByPriceLoHi();
        Product firstProductBeforeSort = this.productList.getFirstProductInList();
        Product lastProductBeforeSort = this.productList.getLastProductInList();
        this.productList.clickOnTheSortButton();
        this.productList.clickOnTheSortByPriceHiLo();
        Product firstProductAfterSort = this.productList.getFirstProductInList();
        Product lastProductAfterSort = this.productList.getLastProductInList();
        Assert.assertEquals(firstProductAfterSort.getPrice(), lastProductBeforeSort.getPrice());
        Assert.assertEquals(lastProductAfterSort.getPrice(), firstProductBeforeSort.getPrice());
    }
}
