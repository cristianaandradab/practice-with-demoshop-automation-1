package org.fasttrackit;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Epic("Login")
@Severity(SeverityLevel.CRITICAL)
@Feature("User can Login to DemoShop App.")
public class DemoShopCartTest {
    Page page = new Page();
    Header header = new Header();

    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickOnTheResetButton();
    }

    @Test
    public void user_can_navigate_to_cart_page() {
        header.clickOnTheCartIcon();
        assertEquals(page.getPageTitle(), "Your cart", "Expected to be on the Cart page");
    }

    @Test(dependsOnMethods = "user_can_navigate_to_cart_page")
    public void user_can_navigate_to_homepage_from_cart_page() {
        header.clickOnTheCartIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on Products page");
    }

    @Test
    public void user_can_add_product_to_cart_from_product_cards() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to cart, badge shows 1.");
    }

    @Test
    public void user_can_add_two_products_to_cart_from_product_cards() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "2", "After adding two products to cart, badge shows 2.");
    }

    @Test
    public void user_can_add_three_products_to_cart_from_product_cards() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "3", "After adding three products to cart, badge shows 3.");
    }


    @Test
    public void user_can_open_product_page_for_product1() {
        Product product = new Product("1");
        final String URL = "https://fasttrackit-test.netlify.app/#/product/1";
        assertEquals("Product id 1 page ", "Product id 1 page ", "Expected to be on the 'Awesome Granite Chips' page");
    }

 @Test
    public void user_turtle_can_open_product_page_for_product1() {
     header.clickOnTheLoginButton();
     modal.typeInUsername("turtle");
     modal.typeInPassword("choochoo");
     modal.clickOnTheLoginButton();
     Product product = new Product("1");
     final String URL = "https://fasttrackit-test.netlify.app/#/product/1";
     assertEquals("Product id 1 page ", "Product id 1 page ", "Expected to be on the 'Awesome Granite Chips' page");
 }
        @Test
    public void user_dino_can_open_product_page_for_product1() {
            header.clickOnTheLoginButton();
            modal.typeInUsername("dino");
            modal.typeInPassword("choochoo");
            modal.clickOnTheLoginButton();
        Product product = new Product("1");
        final String URL = "https://fasttrackit-test.netlify.app/#/product/1";
        assertEquals("Product id 1 page ", "Product id 1 page ", "Expected to be on the 'Awesome Granite Chips' page");



    }
}
