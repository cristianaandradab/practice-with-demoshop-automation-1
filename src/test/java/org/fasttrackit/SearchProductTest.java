

package org.fasttrackit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class SearchProductTest {
    Page page = new Page();
    Header header = new Header();
    ModalDialog modalDialog = new ModalDialog();
    ProductCards productList = new ProductCards();

    public SearchProductTest() {
    }

    @BeforeClass
    public void setup() {
        this.page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickOnTheResetButton();
    }

    @Test
    public void user_can_search_products_by_typing_practical() {
        this.header.clickOnTheSearchBar();
        this.modalDialog.typeInWord("practical");
        this.header.clickOnTheSearchButton();
        assertEquals("Practical", "Practical", "All the products containing the word 'practical' are found");
    }

    @Test
    public void user_can_search_products_by_typing_awesome() {
        this.header.clickOnTheSearchBar();
        this.modalDialog.typeInWord("awesome");
        this.header.clickOnTheSearchButton();
        assertEquals("Awesome", "Awesome", "All the products containing the word 'awesome' are found");
    }

    @Test
    public void user_can_search_one_particular_product_by_typing_the_product_entire_name() {
        this.header.clickOnTheSearchBar();
        this.modalDialog.typeInWord("Incredible Concrete Hat");
        this.header.clickOnTheSearchButton();
        assertEquals("Incredible Concrete Hat", "Incredible Concrete Hat", "Specific product is found");
    }
}

