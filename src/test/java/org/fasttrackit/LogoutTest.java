
package org.fasttrackit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class LogoutTest {

    Page page = new Page();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();


    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickOnTheResetButton();
        header.clickOnTheShoppingBagIcon();
    }


    @Test
    public void user_dino_can_logout_from_the_account() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        header.clickOnTheLogoutButton();
        assertEquals(header.getGreetingsMessage(), "Hello guest!", "Logged out with dino,expected greetings message to be Hello guest!");
    }

    @Test
    public void user_turtle_can_logout_from_the_account() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        header.clickOnTheLogoutButton();
        assertEquals(header.getGreetingsMessage(), "Hello guest!", "Logged out with turtle,expected greetings message to be Hello guest!");
    }

    @Test
    public void user_beetle_can_logout_from_the_account() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("beetle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        header.clickOnTheLogoutButton();
        assertEquals(header.getGreetingsMessage(), "Hello guest!", "Logged out with beetle,expected greetings message to be Hello guest!");


    }

    @Test
    public void user_get_locked_out_when_trying_to_log_in_with_username_locked() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("locked");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        assertEquals("The user has been locked out.", "The user has been locked out.", "User is locked out");
    }



    }
