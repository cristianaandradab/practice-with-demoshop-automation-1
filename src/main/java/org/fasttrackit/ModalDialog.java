package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class ModalDialog {

    private final SelenideElement username = $("#user-name");
    private final SelenideElement password = $("#password");
    private final SelenideElement loginButton = $(".modal-dialog .fa-sign-in-alt");
    private final SelenideElement searchBar = $(" .form-control");

    private final SelenideElement firstName = $("#first-name");
    private final SelenideElement lastName = $("#last-name");
    private final SelenideElement addressButton = $(" #address");

    private final SelenideElement errorElement = $(".error");

    private final SelenideElement close = $(".close");
    public void typeInFirstName(String first) {
        System.out.println("Click on the first name field");
        firstName.click();
        System.out.println("Type in " + first);
        firstName.type(first);
    }

    public void typeInLastName(String last) {
        System.out.println("Click on the last name field");
        lastName.click();
        System.out.println("Type in " + last);
        lastName.type(last);
    }

    public void typeInAddressButton(String address) {
        System.out.println("Click on the address field");
        addressButton.click();
        System.out.println("Type in " + address);
        addressButton.type(address);
    }


    @Step("Type in username")
    public void typeInUsername(String user) {
        System.out.println("Click on the Username field.");
        username.click();
        System.out.println("Type in " + user);
        username.type(user);
    }

    @Step("Type in password")
    public void typeInPassword(String pass) {
        System.out.println("Click on the Password field.");
        password.click();
        System.out.println("Type in " + pass);
        password.type(pass);
    }

    @Step("Click on the login button.")
    public void clickOnTheLoginButton() {
        System.out.println("Click on the Login button.");
        loginButton.click();
    }

    @Step("Click on the search bar.")
    public void clickOnTheSearchBar() {
        System.out.println("Click on the SearchBar.");
        searchBar.click();
    }

    public void typeInWord(String word) {
        System.out.println("Type in" + word);
        searchBar.type(word);
    }

    public void typeInUserName(String turtle) {
    }

    public String getErrorMsg() {
        return this.errorElement.text();
    }

    public void close() {
        this.close.click();
    }

    public boolean isErrorMsgDisplayed() {
        return this.errorElement.isDisplayed();
    }

}