
package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

public class Page {
    public static final String URL = "https://fasttrackit-test.netlify.app";
    private final SelenideElement pageTitle = Selenide.$(".subheader-container .text-muted");

    public Page() {
    }

    @Step("Open Demo shop page.")
    public void openHomePage() {
        System.out.println("Opening: https://fasttrackit-test.netlify.app");
        Selenide.open("https://fasttrackit-test.netlify.app");
    }

    public String getPageTitle() {
        return this.pageTitle.text();
    }
}
