
package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class Header {
    private final SelenideElement loginButton = $(".navbar .fa-sign-in-alt");
    private final SelenideElement logoutButton = $(".navbar .fa-sign-out-alt");
    private final SelenideElement greetingsElement = $(".navbar-text span span");
    private final SelenideElement lockedOutElement = $(" .fa-exclamation-circle");
    private final SelenideElement wishlistButton = $(".navbar .fa-heart");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private final SelenideElement shoppingCartBadge = $(".shopping_cart_badge");
    private final ElementsCollection shoppingCartBadges = Selenide.$$(".shopping_cart_badge");
    private final SelenideElement homePageButton = $("[data-icon=shopping-bag]");
    private final SelenideElement searchBar = $(" .form-control");
    private final SelenideElement searchButton = $(" .btn-light");
    private final SelenideElement checkoutButton = $(" .btn-success");
    private final SelenideElement continueShoppingButton = $(" .btn-danger");
    private final SelenideElement closeButton = $("close");
    private final SelenideElement xButton = $(".close");

    public Header() {
    }

     public void clickOnTheXButton () {
        System.out.println("Click on the X button.");
        xButton.click();
    }


    @Step("Click on the Login button.")
    public void clickOnTheCloseButton() {
        this.closeButton.click();
    }

    public void clickOnTheLoginButton() {
        this.loginButton.click();
        System.out.println("Click on the Login button.");
    }

    public String getGreetingsMessage() {
        return this.greetingsElement.text();
    }

    public void clickOnTheCheckoutButton() {
        System.out.println("click on the checkout button");
        this.checkoutButton.click();
    }

    public void clickOnTheWishlistIcon() {
        System.out.println("Click on the Wishlist button.");
        this.wishlistButton.click();
    }

    public void clickOnTheShoppingBagIcon() {
        System.out.println("Click on the Shopping bag icon.");
        this.homePageButton.click();
    }

    public void clickOnTheCartIcon() {
        System.out.println("Click on the Cart Icon");
        this.cartIcon.click();
    }

    public String getShoppingCartBadgeValue() {
        return this.shoppingCartBadge.text();
    }

    public boolean isShoppingBadgeVisible() {
        return !this.shoppingCartBadges.isEmpty();
    }

    public void clickOnTheLogoutButton() {
        this.logoutButton.click();
        System.out.println("click on the Logout Button");
    }

    public void clickOnTheSearchBar() {
        this.searchBar.click();
        System.out.println("click on the Search Bar");
    }

    public void clickOnTheSearchButton() {
        this.searchButton.click();
        System.out.println("click on Search Button");
    }

    public void clickOnTheContinueShoppingButton() {
        this.continueShoppingButton.click();
        System.out.println("click on Continue Shopping Button");
    }

    public Object getPageTitle() {
        System.out.println("get page title");
        return null;
    }
}
