package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CartPage {
    private final SelenideElement emptyCartPageElement = $(".text-center");
    private final SelenideElement checkoutButton = $(" .btn-success");
    private final SelenideElement checkoutPageInfo = $ (".text-muted");

    public CartPage() {
    }


    public String getCheckoutPageInfo () {
        return this.checkoutPageInfo.text();
    }
    public void clickOnTheCheckoutButton() {
        checkoutButton.click();
        System.out.println("Open Checkout Page");
    }

    public String getEmptyCartPageText() {

        return this.emptyCartPageElement.text();
    }

    public boolean isEmptyCartMessageDisplayed() {

        return emptyCartPageElement.isDisplayed();
    }

    public String getEmptyCheckoutPage() {

        return this.checkoutButton.text();
    }

}
