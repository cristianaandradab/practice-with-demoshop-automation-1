package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

public class Footer {
    private final SelenideElement reset = Selenide.$(".fa-undo");

    public Footer() {
    }

    @Step("Reset app state.")
    public void clickOnTheResetButton() {
        this.reset.click();
    }

    public void clickOnTheRefreshIcon() {
    }

    public void clickOnTheHelpIcon() {
    }
}
